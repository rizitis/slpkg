[<img src="https://gitlab.com/dslackw/slpkg/-/raw/site/docs/images/logo.png" title="slpkg">](https://dslackw.gitlab.io/slpkg)


## About

Slpkg is a software package manager that installs, updates and removes packages on <a href="https://www.slackware.com" target="_blank">Slackware</a>-based systems.
It automatically calculates dependencies and figures out what things need to happen to install packages.
Slpkg makes it easier to manage groups of machines without the need for manual updates.
Slpkg works in accordance with the standards of the <a href="https://www.slackbuilds.org" target="_blank">slackbuilds.org</a> organization to build packages.
It also uses the Slackware Linux instructions for installing, upgrading or removing packages.

## Homepage

Visit the project website [here](https://dslackw.gitlab.io/slpkg/).

## Source

* <a href="https://gitlab.com/dslackw/slpkg" target="_blank">GitLab</a> repository.
* <a href="https://slackbuilds.org/repository/15.0/system/slpkg/" target="_blank">SlackBuilds.org</a> repository.
* <a href="https://sourceforge.net/projects/slpkg/" target="_blank">SourceForge</a> repository.
* <a href="https://pypi.org/project/slpkg/" target="_blank">PyPi</a> repository.

## License

[MIT License](https://dslackw.gitlab.io/slpkg/license/)

## Donate

Did you know that we developers love coffee?

[<img src="https://gitlab.com/dslackw/slpkg/-/raw/site/docs/images/paypaldonate.png" alt="paypal" title="donate">](https://www.paypal.me/dslackw)

## Support

Please support:

* <a href="https://www.patreon.com/slackwarelinux" target="_blank">Slackware</a> project.
* <a href="https://slackbuilds.org/contributors/" target="_blank">SlackBuilds</a> project.
* <a href="https://alien.slackbook.org/blog/" target="_blank">AlienBob</a> project.

Thank you all for your support!

## Copyrights

Slackware® is a Registered Trademark of Patrick Volkerding.
Linux is a Registered Trademark of Linus Torvalds.
